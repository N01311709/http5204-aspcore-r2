﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class SendGift
    {
        [Key]
        public int SendGiftID { get; set; }
        
        [Required, StringLength(100), Display(Name = "Sender Frist Name")]
        public string senderFname { get; set; }

        [Required, StringLength(100), Display(Name = "Sender Last Name")]
        public string senderLname { get; set; }

        [Required, StringLength(100), Display(Name = "E-Mail ")]
        public string Email { get; set; }

        [Required, StringLength(100), Display(Name = "Contact No.")]
        public string Contact { get; set; }

        [Required, StringLength(100), Display(Name = "Patient Name")]
        public string patientName { get; set; }

        [Required, StringLength(7), Display(Name = "Room No.")]
        public string roomNo { get; set; }

        [Required, StringLength(100), Display(Name = "Gift Amount")]
        public string amt { get; set; }

        [Required, StringLength(100), Display(Name = "Message")]
        public string message { get; set; }
        

        //[ForeignKey("AdminID")]
        //public int AdminID { get; set; }

        //public virtual Admin Admin { get; set; }
    }
}
