﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }
        
        [Required, StringLength(255), Display(Name = "First Name")]
        public string AdminFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AdminLName { get; set; }

        [Required, StringLength(255), Display(Name = "Username")]
        public string AdminUsername { get; set; }
        
        [InverseProperty("Admin")]
        public List<Careers> Careers { get; set; }

        [InverseProperty("Admin")]
        public List<Donations> Donations { get; set; }

        [InverseProperty("Admin")]
        public List<Feedback> Feedback { get; set; }

        // one-to-one relationship with ASPNetUsers

        [ForeignKey("UserID")]
        public string UserID { get; set; }

        public virtual ApplicationUser user { get; set; }

        // Margi- FAQ
        [InverseProperty("Admin")]
        public List<FAQ> FAQs { get; set; }

        // Margi - Visitor Information
        [InverseProperty("Admin")]
        public List<VisitorInformation> VisitorInformations { get; set; }

        // Margi - Announcement
        [InverseProperty("Admin")]
        public List<Announcement> Announcements { get; set; }

        //Tarun -Appointment
       // [InverseProperty("Admin")]
       // public List<Appointments> Appointments { get; set; }

        //tarun -send gift
        //[InverseProperty("Admin")]
        //public List<SendGift> sendGifts { get; set; }

        //tarun -program Services
        [InverseProperty("Admin")]
        public List<ProgramServices> programServices { get; set; }


        //many events are associated with this admin
        [InverseProperty("Admin")]
        public List<Event> Events { get; set; }

        //many Emergencies are associated with this admin
        [InverseProperty("Admin")]
        public List<Emergency> Emergencies { get; set; }

        //many NewsAndUpdates are associated with this admin
        [InverseProperty("Admin")]
        public List<NewsAndUpdate> NewsAndUpdates { get; set; }

    }
}
