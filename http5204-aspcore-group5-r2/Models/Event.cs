﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Event
    {

        [Key, ScaffoldColumn(false)]
        public int EventID { get; set; }

        [Required, StringLength(50), Display(Name = "Name")]
        public string EventName { get; set; }

        [Required, Display(Name = "Date")]
        public DateTime EventDate { get; set; }

        [Required, Display(Name = "Time")]
        public DateTime EventTime { get; set; }

        [Required, StringLength(100), Display(Name = "Location")]
        public string EventLocation { get; set; }

        [Required, StringLength(1000), Display(Name = "Detail")]
        public string EventDetail { get; set; }

        //Adding a foreign key
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
