﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Donations
    {
        [Key]
        public int DonationID { get; set; }
        
        [Required, StringLength(100), Display(Name = "Firstname")]
        public string DonationFName { get; set; }

        [Required, StringLength(100), Display(Name = "Last Name")]
        public string DonationLName { get; set; }
        
        [Required, StringLength(100), Display(Name = "Email")]
        public string DonationEmail { get; set; }

        [Required, StringLength(100), Display(Name = "Plan")]
        public string DonationPlan { get; set; }

        [Required, Display(Name = "Amount")]
        public float DonationAmount { get; set; }
        

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
