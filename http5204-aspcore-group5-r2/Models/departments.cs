﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Departments
    {
            [Key]
            public int DeparmentID { get; set; }

            [Required, StringLength(10), Display(Name = "Check or Lab")]
            public string lab { get; set; }

            [Required, StringLength(10),Display(Name ="Caridac")]
            public string Cardiac { get; set; }

            [Required, StringLength(30),Display(Name = "Pelvic & Obstetric Ultrasound")]
            public string Pelvic { get; set; }

            [Required, StringLength(20),Display(Name ="Gastrointestinal(GI) Series")]
            public string GI { get; set; }

            [Required, StringLength(20), Display(Name = " Abdomen & Pelvic Ultrasound")]
            public string Abdomen { get; set; }

            [Required, StringLength(20), Display(Name = "Intravenous Pyelogram(IVP)")]
            public string IVP { get; set; }
       

                //will change to user ID
                [ForeignKey("AdminID")]
                public int AdminID { get; set; }

                public virtual Admin Admin { get; set; }
    }
}


