﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Appointments
    {

        [Key]
        public int AppointmentID { get; set; }

        
        [Required, StringLength(100), Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(100), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required, StringLength(100), Display(Name = "E-Mail ")]
        public string Email { get; set; }

        [Required, StringLength(100), Display(Name = "Contact No.")]
        public string Contact { get; set; }

        [Required, StringLength(100), Display(Name = "Address")]
        public string Address { get; set; }

        [Required, StringLength(7), Display(Name = "PostalCode")]
        public string Postal { get; set; }

        [Required, StringLength(100), Display(Name = "Department Name ")]
        public string DepartmentName { get; set; }

        [Required, DataType(DataType.DateTime), Display(Name = "Preferred Date")]
        public string Pdate { get; set; }

        //[Required, DataType(DataType.DateTime), Display(Name = "Preferred Time")]
        //public string Ptime { get; set; }

        [Required, StringLength(100), Display(Name = "Communication")]
        public string Communication { get; set; }

    
        //[ForeignKey("AdminID")]
       // public int AdminID { get; set; }

        //public virtual Admin Admin { get; set; }
    }
}
