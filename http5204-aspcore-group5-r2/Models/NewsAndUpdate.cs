﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class NewsAndUpdate
    {
        [Key, ScaffoldColumn(false)]
        public int NewsUpdateID { get; set; }

        [Required, StringLength(50), Display(Name = "Title")]
        public string NewsUpdateTitle { get; set; }

        //[Required, Display(Description = "Image")]
        //public string image { get; set; }

        //profile picture
        //1=>picture exists (in imgs/newsUpdate/{id}.img)
        //0=>picture doesn't exist
        public int NewsUpdateImage { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Description")]
        public string NewsUpdateDescription { get; set; }

        //[Required, StringLength(400), Display(Name = "Description")]
        //public string Description { get; set; }

        //Adding a foreign key

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }

    }
}
