﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace http5204_aspcore_group5_r2.Models
{
    public class Announcement
    {
        //for Announcement ID
        [Key]
        public int AnnouncementID { get; set; }

        
        //for Announcement Profile Image(Banner Image)
        //1=>picture exists (in wwwroot/images/announcements/{id}.img)
        //0=>picture doesn't exist
        public int HasPic { get; set; }

        //accepted image file formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        //for Announcement Title
        [Required, StringLength(255), Display(Name = "Title")]
        public string AnnouncementTitle { get; set; }

        //for Announcement Description
        [StringLength(int.MaxValue), Display(Name = "Description")]
        public string AnnouncementDescription { get; set; }


        //for Announcement Type(Post Type)
        //Announcenment Type will have three Types: News, Media, Alert (I do not know for media and alert)
        //one News can have many Alerts 
        //see Tags in Chritine's Code
        // [ForeignKey("NewsID")]
        // public int? NewsID { get; set; }

        // public virtual News News { get; set; }


        [Required, StringLength(255), Display(Name = "Announcement Type")]
        public string AnnouncementType { get; set; }



        //for Destination URL/Link (For Ex. Learn More Link)
        [Required, StringLength(int.MaxValue), Display(Name = "Destination Link")]
        public string DestinationLink { get; set; }
        

        //for Foregin Key Admin ID (AnnouncementAdmin)
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
 