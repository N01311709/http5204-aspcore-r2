﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Spiritual
    {
        [Key]
        public int SpiritualID { get; set; }
        
        [Required, StringLength(255), Display(Name = "Title")]
        public string SpiritualTitle { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string SpiritualFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string SpiritualLName { get; set; }

        [Required, StringLength(500), Display(Name = "Denomination")]
        public string SpiritualDenomination { get; set; }

        [Required, StringLength(500), Display(Name = "Establishment")]
        public string SpiritualEstablishment { get; set; }
        

        //will change to user ID
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
