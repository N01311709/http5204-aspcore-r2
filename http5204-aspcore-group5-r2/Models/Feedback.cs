﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace http5204_aspcore_group5_r2.Models
{
    public class Feedback
    {
        [Key]
        public int FeedbackID { get; set; }

        
        [Required, StringLength(100), Display(Name = "Title")]
        public string FeedbackTitle { get; set; }

        [Required, Display(Name = "Date")]
        public DateTime FeedbackDate { get; set; }

        public bool IsPublished { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Review")]
        public string FeedbackMsg { get; set; }
        
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }

    }
}
