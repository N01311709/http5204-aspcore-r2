﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace http5204_aspcore_group5_r2.Models
{
    public class FAQ
    {
        //for FAQ Question ID
        [Key]
        public int FAQQuestionID { get; set; }
        
        //for FAQ Question Title
        [Required, StringLength(255), Display(Name = "Title")]
        public string FAQQuestionTitle { get; set; }
    
        //for Answer
        [StringLength(int.MaxValue), Display(Name = "Answer")]
        public string FAQAnswer { get; set; }
        
        //FAQ has Admin ID (Admin can create multiple FAQs)
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        //FAQ Admin
        public virtual Admin Admin { get; set; } 
    }
}

