﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Contact
    {
        [Key]
        public int ContactID { get; set; }
        
        [Required, StringLength(100)]
        public string ContactReason { get; set; }

        [Required, StringLength(500)]
        public string ContactMessage { get; set; }

        [Required, StringLength(500)]
        public string ContactName { get; set; }
        
        [Required]
        public string ContactPhone { get; set; }

        [Required]
        public string ContactEmail { get; set; }
        
        //public bool IsArchived { get; set; }

        //will change to user ID
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
