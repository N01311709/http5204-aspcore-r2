﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Careers
    {
        [Key]
        public int CareerID { get; set; }
        
        [Required, StringLength(100), Display(Name = "Title")]
        public string CareerTitle { get; set; }

        [Required, StringLength(100), Display(Name = "Description")]
        public string CareerDesc { get; set; }

        [Required, StringLength(100), Display(Name = "Department")]
        public string CareerDepartment { get; set; }

        public string ResumeUpload { get; set; }
        
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
