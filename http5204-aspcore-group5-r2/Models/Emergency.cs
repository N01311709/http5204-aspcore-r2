﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class Emergency
    {

        [Key, ScaffoldColumn(false)]
        public int EmergencyID { get; set; }


        [Required, StringLength(100), Display(Name = "Location")]
        public string EmergencyLocation { get; set; }

        [Required, StringLength(10), Display(Name = "Contact Number")]
        public string EmergencyContactNo { get; set; }

        [Required, StringLength(70), Display(Name = "Email")]
        public string EmergencyEmail { get; set; }

        //Adding a foreign key
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
