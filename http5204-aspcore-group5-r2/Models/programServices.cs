﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class ProgramServices
    {
        [Key]
        public int programServiceID { get; set; }

        
        [Required, StringLength(100), Display(Name = "Title")]
        public string Title { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }    
    }
}
