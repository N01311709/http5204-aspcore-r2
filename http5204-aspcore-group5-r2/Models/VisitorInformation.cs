﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_aspcore_group5_r2.Models
{
    public class VisitorInformation
    {
        //for Visitor ID
        [Key]
        public int VisitorID { get; set; }

        //for VisitorName
        [Required, StringLength(255), Display(Name = "Name")]
        public string VisitorName { get; set; }

        //for Visiting Person Name(A Visitor visting a Person)
        [Required, StringLength(255), Display(Name = "Person Visiting")]
        public string VisitingPersonName { get; set; }

        //for Date Reference: https://stackoverflow.com/questions/43752832/format-date-within-view-in-asp-net-core-mvc
        //[Required, DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"),Display(Name ="Date")]
        [Required, Display(Name = "Date")]
        public DateTime Date { get; set; }

        //for Time-In Reference: https://stackoverflow.com/questions/24549745/allow-only-hhmm-format-for-datetime-field-in-asp-net-mvc
        //This will be in /New (Page) When Visitor will be sign in 
        //[DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true),Display(Name ="Time-In")]
        //[DataType(DataType.Time)]
        [Display (Name = "TimeIn")]
        public DateTime TimeIn { get; set; }

        //for Time-Out 
        //This will be in Edit becuase after met when he will leaves
        //[DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true), Display(Name = "Time-Out")]
        //[DataType(DataType.Time)]
        [Display(Name = "TimeOut")]
        public DateTime? TimeOut { get; set; }

        //Visitor Information has Admin ID (Admin can create multiple VisitorInformations)
        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
 