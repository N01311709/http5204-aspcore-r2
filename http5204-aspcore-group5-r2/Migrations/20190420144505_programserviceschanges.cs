﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace http5204_aspcore_group5_r2.Migrations
{
    public partial class programserviceschanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "ProgramServices");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "ProgramServices",
                newName: "Title");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ProgramServices",
                maxLength: 2147483647,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "ResumeUpload",
                table: "Careers",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "ProgramServices");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "ProgramServices",
                newName: "LastName");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "ProgramServices",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "ResumeUpload",
                table: "Careers",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
