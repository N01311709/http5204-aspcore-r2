﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

// Entity Framework Code
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;

//model data
using http5204_aspcore_group5_r2.Models;


namespace http5204_aspcore_group5_r2.Data
{
    public class NDMHCMSContext : IdentityDbContext<ApplicationUser>
    {
        public NDMHCMSContext(DbContextOptions<NDMHCMSContext> options)
            : base(options)
        {
        }

        public DbSet<Admin> Admin { get; set; }
        public DbSet<Careers> Careers { get; set; }
        public DbSet<Donations> Donations { get; set; }
        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<Spiritual> Spiritual { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Emergency> Emergencies { get; set; }
        public DbSet<NewsAndUpdate> NewsAndUpdates { get; set; }


        //margi-FAQ
        public DbSet<FAQ> FAQs { get; set; }

        //margi-Visitor Information
        public DbSet<VisitorInformation> VisitorInformations { get; set; }

        //margi-Announcement
        public DbSet<Announcement> Announcements { get; set; }
        
        //Appointments (Tarun )
        public DbSet<Appointments> Appointments { get; set; }

        //send gifts (Tarun )
        public DbSet<SendGift> SendGifts { get; set; }

        //Appointments (Tarun )
        public DbSet<ProgramServices> programServices { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Careers>()
                .HasOne(c => c.Admin)
                .WithMany(a => a.Careers)
                .HasForeignKey(c => c.AdminID);

            modelBuilder.Entity<Careers>()
                .HasKey(c => c.CareerID);

            modelBuilder.Entity<Donations>()
                .HasOne(d => d.Admin)
                .WithMany(a => a.Donations)
                .HasForeignKey(d => d.AdminID);

            modelBuilder.Entity<Donations>()
                .HasKey(d=>d.DonationID);

            modelBuilder.Entity<Feedback>()
            .HasOne(f => f.Admin)
            .WithMany(a => a.Feedback)
            .HasForeignKey(f => f.AdminID);

            modelBuilder.Entity<Feedback>()
            .HasKey(f=>f.FeedbackID);

            //margi Added one to one relationship between user(as a Parent) and admin(as a Child), And one Admin can have one User and one user is only one Admin
            //Reference: https://stackoverflow.com/questions/43546825/fluent-api-entity-framework-core
            //Reference: Christine's Code
            modelBuilder.Entity<Admin>()
                .HasOne(a => a.user)
                .WithOne(u => u.admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminID);

            modelBuilder.Entity<Admin>()
                .HasKey(a => a.AdminID);


            //Admin can create Many FAQs
            modelBuilder.Entity<FAQ>()
                .HasOne(f=>f.Admin)
                .WithMany(a=>a.FAQs)
                .HasForeignKey(f=>f.AdminID);

            //Admin can create Many Visitor Informaiotns
            modelBuilder.Entity<VisitorInformation>()
                .HasOne(v => v.Admin)
                .WithMany(a => a.VisitorInformations)
                .HasForeignKey(v => v.AdminID);

            modelBuilder.Entity<VisitorInformation>()
                .HasKey(v => v.VisitorID);
            
            //Admin can create many Annoucements(posts), each post has only one Admin
            modelBuilder.Entity<Announcement>()
                .HasOne(p => p.Admin)
                .WithMany(a => a.Announcements)
                .HasForeignKey(p => p.AdminID);

            modelBuilder.Entity<Announcement>()
                .HasKey(a => a.AnnouncementID);

            // Admin can create, update and delete program and services
            //modelBuilder.Entity<Appointments>()
            //    .HasOne(p => p.Admin)
            //    .WithMany(a => a.Appointments)
            //    .HasForeignsKey(p => p.AdminID);

            modelBuilder.Entity<Appointments>()
                .HasKey(a => a.AppointmentID);

            //admin can create many programs
            modelBuilder.Entity<ProgramServices>()
                .HasOne(p => p.Admin)
                .WithMany(a => a.programServices)
                .HasForeignKey(p => p.AdminID);

            modelBuilder.Entity<ProgramServices>()
                .HasKey(p => p.programServiceID);

            //admin can create many programs

            //modelBuilder.Entity<SendGift>()
            //.HasOne(p => p.Admin)
            //.WithMany(a => a.sendGifts)
            //.HasForeignKey(p => p.AdminID);

            modelBuilder.Entity<SendGift>()
                .HasKey(a => a.SendGiftID);

            //modelBuilder.Entity<SendGift>()
            //    .HasKey(s => s.SendGiftID);


            modelBuilder.Entity<Event>()
           .HasOne(e => e.Admin)
           .WithMany(a => a.Events)
           .HasForeignKey(e => e.AdminID);
            modelBuilder.Entity<Event>()
           .HasKey(e => e.EventID);

            modelBuilder.Entity<Emergency>()
           .HasOne(em => em.Admin)
           .WithMany(a => a.Emergencies)
           .HasForeignKey(e => e.AdminID);
            modelBuilder.Entity<Emergency>()
           .HasKey(e => e.EmergencyID);

            modelBuilder.Entity<NewsAndUpdate>()
           .HasOne(nu => nu.Admin)
           .WithMany(a => a.NewsAndUpdates)
           .HasForeignKey(e => e.AdminID);
            modelBuilder.Entity<NewsAndUpdate>()
           .HasKey(e => e.NewsUpdateID);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Admin>().ToTable("Admin");
            modelBuilder.Entity<Careers>().ToTable("Careers");
            modelBuilder.Entity<Donations>().ToTable("Donations");
            modelBuilder.Entity<Feedback>().ToTable("Feedback");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<Emergency>().ToTable("Emergency");
            modelBuilder.Entity<NewsAndUpdate>().ToTable("NewsAndUpdates");



            //margi-FAQ
            modelBuilder.Entity<FAQ>().ToTable("FAQs");

            //margi-Visitor Information
            modelBuilder.Entity<VisitorInformation>().ToTable("VisitorInformations");

            //margi-Announcenment
            modelBuilder.Entity<Announcement>().ToTable("Announcements");

            //tarun (Request for Appointment)
            modelBuilder.Entity<Appointments>().ToTable("Appointments");

            //send Gift (tarun)
            modelBuilder.Entity<SendGift>().ToTable("SendGifts");
                                                          
            //program
            modelBuilder.Entity<ProgramServices>().ToTable("ProgramServices");


            modelBuilder.Entity<Contact>().ToTable("Contacts");
            modelBuilder.Entity<Spiritual>().ToTable("Spirituals");
        }

    }
}