﻿//REF Chrsitine code : blog asp.net core
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class EmergencyController : Controller
    {

        private readonly NDMHCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public EmergencyController(NDMHCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        /* public IActionResult Index()
         {
             return View();
         }
         */

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uAdminId = user.AdminID;
                var userEmegrency = await db.Emergencies.Where(e => e.AdminID == uAdminId).ToListAsync();
                var userEmergecnycount = userEmegrency.Count();
                if (userEmergecnycount == 0) return 2;
                else if (userEmergecnycount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserEmergencyOwner(ApplicationUser user, int EmergencyID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;

            var userEmegency = await
                db
                .Emergencies
                .Include(em => em.Admin)
                .Where(em => em.AdminID == user.AdminID)
                .Where(em => em.EmergencyID == EmergencyID)
                .ToListAsync();
            if (userEmegency.Count() > 0) return true;
            return false;
        }

        //Index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        /**************************************User view for the vent feature**********************************************/
        public async Task<ActionResult> List(int pagenum)
        {
            // Pagination
            var _EmergencyPage = await db.Emergencies.ToListAsync();
            int emergencyCount = _EmergencyPage.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)emergencyCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<Emergency> emergencies = await db.Emergencies.Include(e => e.Admin).Skip(start).Take(perpage).ToListAsync();
            return View(emergencies);



          
        }
        /*************************************Admin table view (Only admin can see this view after he logges in)***********************************************************/
        public async Task<ActionResult> Show()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            //return View();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.Emergencies.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Emergencies.ToListAsync());
            }

            // return RedirectToAction("List");
        }


        /*********************************Create new Emergency *********************************************/
        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(string Emergency_Location, string Emergency_Contact, string Emergency_Email, int Emergency_AdminID)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate < 2) return Forbid();
            if (user.AdminID != Emergency_AdminID)
            {
                //someone tried to impersonate a blog user by switching the hidden field ID
                return Forbid();
            }
            //SQL Query  for Emergency
            string query = "insert into Emergency (EmergencyLocation,EmergencyContactNo,EmergencyEmail,AdminID) " +
                "values (@location, @contact,@email,@admin)";

            SqlParameter[] myparams = new SqlParameter[4];
            //for @title paramter
            myparams[0] = new SqlParameter("@location", Emergency_Location);
            //for @conact parameter
            myparams[1] = new SqlParameter("@contact", Emergency_Contact);
            // for @email paramter
            myparams[2] = new SqlParameter("@email", Emergency_Email);
            //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)
            myparams[3] = new SqlParameter("@admin", Emergency_AdminID);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show");

        }
        /********************************Edit particuler data **************************************************************************/
        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserEmergencyOwner(user, id);
            if (!isValid) return Forbid();


            var edit_emergency = db.Emergencies.Find(id); //finds that blog
            if (edit_emergency != null) return View(edit_emergency);
            else return NotFound();
        }
        [HttpPost]
        public async Task<ActionResult> Edit(int id, string emergency_location, string emergency_email, string emergency_Num)
        {
            if (db.Emergencies.Find(id) == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserEmergencyOwner(user, id);

                if (!isValid) return Forbid();

                //Query to edit particuler emergency
                string query = "update Emergency set EmergencyLocation=@location, EmergencyEmail=@email,  EmergencyContactNo=@contact " +
                            "where EmergencyID=@id";

                SqlParameter[] myparams = new SqlParameter[4];
                //Parameter for @location "emergency_location"
                myparams[0] = new SqlParameter("@location", emergency_location);

                //Parameter for @location "emergency_email"
                myparams[1] = new SqlParameter("@email", emergency_email);

                //Parameter for (admin) id FOREIGN KEY
                myparams[2] = new SqlParameter("@contact", emergency_Num);

                myparams[3] = new SqlParameter("@id", id);

                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

                //GOTO: View/Emergency/Show.cshtml with paramter 
                return RedirectToAction("Show/" + id);
            }

            //something went wrong
            return Forbid();

        }
        /*****************************Detail view of Emergency****************************************************************/
       /* public async Task<ActionResult> Detail(int id)
        {
            if (db.Emergencies.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["IsUserEmergencyOwner"] = false;
            if (userstate == 3)
            {
                ViewData["IsUserEmergencyOwner"] = await IsUserEmergencyOwner(user, id);
            }
            Emergency detail_emergency = await db.Emergencies.SingleOrDefaultAsync(e => e.EmergencyID == id);
            return View(detail_emergency);

        }*/
        /*****************************Delete*******************************************/
        public async Task<ActionResult> Delete(int id)
        {
            if (db.Emergencies.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserEmergencyOwner(user, id);
                if (!isValid) return Forbid();
                string query = "delete from Emergency where EmergencyID = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);

                return RedirectToAction("List");
            }
            //Something went wrong...
            return Forbid();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}

