﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class AdminController : Controller
    {
        private readonly NDMHCMSContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AdminController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState:
            //0 => No account whatsoever
            //1 => Is User; Patient Online Services account
            //2 => Is an Admin account
            if (user == null) return 0; // only is a User, not an admin (i.e. Patient account only)
            var userid = user.Id;
            if (user.AdminID == null) return 1; //Only a User; not hospital staff
            else
            {
                return 2; //User has a staff/admin account and is a hospital employee

            }

        }

        // redoing GET according to Christine's example. below was the default
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var _accounts = await db.Admin.ToListAsync();
            int acctcount = _accounts.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)acctcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pnum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Admin> admins = await db.Admin.Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["UserAdminID"] = 0;
            if (userstate==2)
            {
                ViewData["UserAdminID"] = user.AdminID;
            }
            return View(await db.Admin.ToListAsync());

            // Former process using "UserHasAdmin" token
            //if (user != null)
            //{
            //    if (user.AdminID == null) {
            //        ViewData["UserHasAdmin"] = "False";
            //    }
            //    else {
            //        ViewData["UserHasAdmin"] = user.AdminID.ToString();
            //    }
            //    return View(await db.Admin.ToListAsync());
            //}
            //else
            //{
            //    ViewData["UserHasAdmin"] = "None";
            //    return View(await db.Admin.ToListAsync());
            //}

        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Details/" + id);
        }

        // GET: Admin/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            var user = await GetCurrentUserAsync();

            if (id == null)
            {
                return new StatusCodeResult(400);
            }

            //Author located_author = await db.Authors.Include(a=>a.Blogs).SingleOrDefaultAsync(a=>a.AuthorID==id);
            Admin located_admin = await db.Admin.SingleOrDefaultAsync(a => a.AdminID == id);
            if (located_admin == null)
            {
                return NotFound();
            }
            if (user != null)
            {
                if (user.admin == located_admin)
                {
                    ViewData["UserHasAdmin"] = "True";
                }
                else
                {
                    ViewData["UserHasAdmin"] = "False";
                }
            }
            else
            {
                ViewData["UserHasAdmin"] = "False";
            }


            return View(located_admin);
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("AdminID,AdminFName,AdminLName,AdminUsername")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                db.Admin.Add(admin);
                db.SaveChanges();
                var res = await MapUserToAdmin(admin);
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        // GET: Admin/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin admin = db.Admin.Find(id);
            if (admin == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); 
            if (user.AdminID != id)
            {
                return Forbid();
            }
            return View(admin);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminID,AdminFName,AdminLName,AdminUsername")] Admin admin)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if (user.AdminID != admin.AdminID)
            {
                return Forbid();
            }
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(admin);
        }

        // GET: Admin/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }

            var admin = await db.Admin.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if (user.AdminID != id)
            {
                return Forbid();
            }
            return View(admin);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Admin admin = await db.Admin.FindAsync(id);
            var user = await GetCurrentUserAsync();
            if (user.AdminID != id)
            {
                return Forbid();
            }
            await UnmapUserFromAdmin(id);
            db.Admin.Remove(admin);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // unmapping/mapping functions regulating the foreign key relationships between users and admins
        
        public async Task<IActionResult> UnmapUserFromAdmin(int id)
        {
            Admin admin = await db.Admin.FindAsync(id);
            admin.user = null;
            admin.UserID = "";
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res == 0)
                {
                    return BadRequest(admin_res);
                }
                else
                {
                    var user = await GetCurrentUserAsync();
                    user.admin = null;
                    user.AdminID = null;
                    var user_res = await _userManager.UpdateAsync(user);
                    if (user_res == IdentityResult.Success)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest(user_res);
                    }
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }

        }

        private async Task<IActionResult> MapUserToAdmin(Admin admin)
        {
            var user = await GetCurrentUserAsync();
            user.admin = admin;
            var user_res = await _userManager.UpdateAsync(user);
            if (user_res != IdentityResult.Success)
            {
                return BadRequest(user_res);
            }
            admin.user = user;
            admin.UserID = user.Id;
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(admin_res);
                }
            }
            else
            {
                return BadRequest("Unstable Admin Model.");
            }
        }
    }
}