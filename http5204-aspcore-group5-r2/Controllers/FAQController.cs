﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;
//Christine's Reference
namespace http5204_aspcore_group5_r2.Controllers
{
    public class FAQController : Controller
    {

        private readonly NDMHCMSContext db;

        //margi added
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FAQController(NDMHCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        /* public IActionResult Index()
         {
             return View();
         }
         */

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an Admin or not.
            //UserState
            //0 => No User
            //1 => has user has no admin
            //2 => has user has admin but no FAQs
            //3 => has user has admin and at least 1 FAQ
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AdminID == null) return 1; //User has no Admin
            else
            {
                var uadminrid = user.AdminID;
                var userfaqs = await db.FAQs.Where(f => f.AdminID == uadminrid).ToListAsync();
                var userfaqcount = userfaqs.Count();
                if (userfaqcount == 0) return 2;//User has no FAQs
                else if (userfaqcount > 0) return 3; //User has atleast one FAQ
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserFaqOwner(ApplicationUser user, int FAQQuestionID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;
            //purpose of this function is to check whether the user is the author of the blog
            var userfaqs = await
                db
                .FAQs
                .Include(f => f.Admin)
                .Where(f => f.AdminID == user.AdminID)
                .Where(f => f.FAQQuestionID == FAQQuestionID)
                .ToListAsync();
            if (userfaqs.Count() > 0) return true;
            return false;
        }

        //Index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //List
        /* public ActionResult List()
         {
            List<FAQ> faqs = db.FAQs.Include(f=>f.Admin).ToList();

             return View(faqs);
         }
         */

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //tell give us context about the user
            ViewData["UserState"] = userstate;
            ViewData["UserHasAdmin"] = 0; //default 0
            if (userstate == 3)
            {
                ViewData["UserHasAdmin"] = (int)user.AdminID;
            }


            //Pagination
            var listfaqs = await db.FAQs.Include(f => f.Admin).ToListAsync();
            int faqcount = listfaqs.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)faqcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<FAQ> faqs = await db.FAQs.Include(f => f.Admin).Skip(start).Take(perpage).ToListAsync();



            /* var user = await GetCurrentUserAsync();

             if (user != null)
             {
                 //if the user has no Admin let the view know 
                 if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                 //if the user has an Admin then let the view know
                 else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                 return View(faqs);
             }
             else
             {
                 //tell the view there is no Admin
                 ViewData["UserHasAdmin"] = "None";
                 return View(faqs);
             } 
             /*Margi changed for Edit and Delete button FAQ for that Admin on List Page*/
              
            return View(faqs);
            //GOTO Views/FAQ/List.cshtml

        }
        

        public async Task<ActionResult> New()
        {
            //DATA NEEDED
            //we need to know the context of the user
            //are they registered as an author?
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //userstate gets the context of the user
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            //The user should have an author if not 0 or 1
            ViewData["UserHasAdmin"] = user.AdminID;
            //Model defined in Models/ViewModels/BlogEdit.cs
           // BlogEdit blogeditview = new BlogEdit();

            //GOTO Views/Blog/New.cshtml
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> New(string FAQQuestionTitle_New, string FAQAnswer_New, int FAQAdmin_New)
        {
            //need to check if the author is indeed who they say they are.
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Can't make a blog if you aren't an author or don't have an account
            if (userstate < 2) return Forbid();
            if (user.AdminID != FAQAdmin_New)
            {
                //someone tried to impersonate a blog user by switching the hidden field ID
                return Forbid();
            }

            //Raw Query   
            string query = "insert into faqs (FAQQuestionTitle, FAQAnswer, AdminID) values (@title, @answer, @admin)";

            //SQL parameterized query technique
            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 3 items
            SqlParameter[] myparams = new SqlParameter[3];
            //for @title paramter
            myparams[0] = new SqlParameter("@title", FAQQuestionTitle_New);
            //for @answer parameter
            myparams[1] = new SqlParameter("@answer", FAQAnswer_New);
            //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)
            myparams[2] = new SqlParameter("@admin", FAQAdmin_New);

           
            db.Database.ExecuteSqlCommand(query, myparams);

           
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserFaqOwner(user, id);
            if (!isValid) return Forbid();


            var myfaq = db.FAQs.Find(id); //finds that blog
            if (myfaq != null) return View(myfaq);
            else return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, string FAQQuestionTitle, string FAQAnswer, int FAQAdmin)
        {
           
            if (db.FAQs.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserFaqOwner(user, id);
                if (!isValid) return Forbid();
                //If Something is going to right

                //Raw Update MSSQL query
                string query = "update faqs set FAQQuestionTitle=@title, FAQAnswer=@answer, AdminID=@admin where FAQQuestionID=@id";


                SqlParameter[] myparams = new SqlParameter[4];
                //Parameter for @title "FAQQuestionTitle"
                myparams[0] = new SqlParameter("@title", FAQQuestionTitle);
                //Parameter for @answer "FAQAnswer"
                myparams[1] = new SqlParameter("@answer", FAQAnswer);
                //Parameter for (admin) id FOREIGN KEY for FAQAdmin
                myparams[2] = new SqlParameter("@admin", FAQAdmin);
                //Pararameter for (faq) id PRIMARY KEY
                myparams[3] = new SqlParameter("@id", id);

                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

               
                return RedirectToAction("Show/" + id);
            }
            //if something is going to wrong
            return Forbid();

        }

        //for show view

        public async Task<ActionResult> Show(int id)
          { /*
              if ((id == null) || (db.FAQs.Find(id) == null))
              {
                  return NotFound();

              }

              var user = await GetCurrentUserAsync();
              if (user != null)
              {
                  
                  if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
        
                  else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

              }

              else
              {
                  //tell the view there is no user
                  ViewData["UserHasAdmin"] = "None";

              }


              //Raw MSSQL query
              string query = "select * from faqs where FAQQuestionID=@id";


              SqlParameter myparam = new SqlParameter("@id", id);
            //margi comented this for more functionality on show page for edit and delete button*/
            if (db.FAQs.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["IsUserFaqOwner"] = false;
            if (userstate == 3)
            {
                ViewData["IsUserFaqOwner"] = await IsUserFaqOwner(user, id);
            }

            FAQ myfaq = db.FAQs.Include(f => f.Admin).SingleOrDefault(f => f.FAQQuestionID == id);

              return View(myfaq);


          }


       

        public async Task<ActionResult> Delete(int id)
        {
           
            if (db.FAQs.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserFaqOwner(user, id);
                if (!isValid) return Forbid();
               
                //delete faq
                string query = "delete from faqs where FAQQuestionID=@id";
                // SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));
                // await db.Database.ExecuteSqlCommandAsync(query, param);
                return RedirectToAction("List");
            }
           
            return Forbid();
        }


        protected override void Dispose(bool disposing)
          {
              if (disposing)
              {
                  db.Dispose();
              }
              base.Dispose(disposing);
        }
    }
}
     