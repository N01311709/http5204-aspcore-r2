﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class SpiritualController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public SpiritualController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {

            db = context;

            _env = env;

            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> New()
        {

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(string title, string fname, string lname, string denomination, string establishment)
        {
            string query = "insert into Spiritual (SpiritualTitle, SpiritualFName, SpiritualLName, SpiritualDenomination, SpiritualEstablishment) " +
                "values (@title, @fname, @lname, @denomination, @establishment)";


            SqlParameter[] myparams = new SqlParameter[5];

            myparams[0] = new SqlParameter("@title", title);

            myparams[1] = new SqlParameter("@fname", fname);

            myparams[2] = new SqlParameter("@lname", lname);

            myparams[3] = new SqlParameter("@denomination", denomination);

            myparams[4] = new SqlParameter("@establishment", establishment);


            db.Database.ExecuteSqlCommand(query, myparams);


            return RedirectToAction("List");
        }

        //pagination
        public async Task<ActionResult> List(int pagenum)
        {
            var SpiritualList = await db.Spiritual.ToListAsync();
            int reviews = SpiritualList.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)reviews / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["Pagination"] = "";
            if (maxpage > 0)
            {
                ViewData["Pagination"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<Spiritual> spiritual = await db.Spiritual.Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(spiritual);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(spiritual);
            }

        }

        public ActionResult Edit(int? id)
        {
            Spiritual spiritual = new Spiritual();
            spiritual =
                db.Spiritual.SingleOrDefault(p => p.SpiritualID == id);
            if (spiritual != null) return View(spiritual);
            else return NotFound();
        }

        [HttpPost]
        public ActionResult Edit(int? id, string title, string fname, string lname, string denomination, string establishment)
        {
            if ((id == null) || (db.Spiritual.Find(id) == null))
            {
                return NotFound();

            }
            string query = "update Spiritual set SpiritualTitle=@title, " +
                "SpiritualFName=@fname, " +
                "SpiritualLName=@lname," +
                "SpiritualDenomination=@denomination," +
                "SpiritualEstablishment=@establishment where SpiritualID = @id";

            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@title";
            myparams[0].Value = title;

            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@fname";
            myparams[1].Value = fname;

            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@lname";
            myparams[2].Value = lname;

            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@denomination";
            myparams[3].Value = denomination;

            myparams[4] = new SqlParameter();
            myparams[4].ParameterName = "@establishment";
            myparams[4].Value = establishment;

            myparams[5] = new SqlParameter();
            myparams[5].ParameterName = "@id";
            myparams[5].Value = id;
            db.Database.ExecuteSqlCommand(query, myparams);
 
            return RedirectToAction("List");
        }

        //Delete
        public ActionResult Delete(int id)
        {
            string sql = "delete from Spiritual where SpiritualID = @id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }
    }
}