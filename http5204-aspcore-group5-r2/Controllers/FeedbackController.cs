﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FeedbackController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {

            db = context;

            _env = env;

            _userManager = usermanager;
        }
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState:
            //0 => No account whatsoever
            //1 => Is User; Patient Online Services account
            //2 => Is an Admin account
            if (user == null) return 0; // only is a User, not an admin (i.e. Patient account only)
            var userid = user.Id;
            if (user.AdminID == null) return 1; //Only a User; not hospital staff
            else
            {
                return 2; //User has a staff/admin account and is a hospital employee

            }

        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> New()
        {

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(db.Feedback.ToList());
        }

        [HttpPost]
        public ActionResult Create(int FeedbackAdmin, string fbTitle, string fbDate, string fbContent)
        {
            //string date = DateTime.Now.ToString("dd/MM/yy");

            string query = "insert into feedback (AdminID, FeedbackTitle, FeedbackDate, FeedbackMsg, IsPublished) values (@id, @title, @date, @content, @ispub)";


            SqlParameter[] myparams = new SqlParameter[5];

            myparams[0] = new SqlParameter("@id", FeedbackAdmin);

            myparams[1] = new SqlParameter("@title", fbTitle);

            myparams[2] = new SqlParameter("@date", fbDate);

            myparams[3] = new SqlParameter("@content", fbContent);

            myparams[4] = new SqlParameter("@ispub", false);


            db.Database.ExecuteSqlCommand(query, myparams);


            return RedirectToAction("List");
        }

        //Change to admin list
        public async Task<ActionResult> List(int pagenum)
        {
            var fbList = await db.Feedback.ToListAsync();
            int reviews = fbList.Count();

            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)reviews / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["Pagination"] = "";
            if (maxpage > 0)
            {
                ViewData["Pagination"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            //Get list based off of user access
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            if (userstate == 2)
            {
                //if user is an admin they will see all posts
                ViewData["UserHasAdmin"] = "True";
                // string query = "SELECT * FROM feedback";
                //db.Database.ExecuteSqlCommand(query);
                List<Feedback> feedback = await db.Feedback.Include(f => f.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(feedback);
            }
            else
            {
                //if user is any rank other than admin, they will only see published posts
                ViewData["UserHasAdmin"] = "False";

             
       
                List<Feedback> feedback = await db.Feedback.Where(f => f.IsPublished.Equals(true)).Include(f => f.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(feedback);
            }


        }

       

        //Show
        public async Task<ActionResult> Show(int? id)
        {
            //if ((id == null) || (db.Feedback.Find(id) == null))
            //{
            //    return NotFound();
            //}

            SqlParameter myparam = new SqlParameter("@id", id);

            Feedback post = db.Feedback.Include(f => f.Admin).SingleOrDefault(f => f.FeedbackID == id);

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            if (userstate == 2)   
                {
                    ViewData["UserHasAdmin"] = "True";
                }
                else
                {
                    ViewData["UserHasAdmin"] = "False";
                }
            
     
            //var user = await GetCurrentUserAsync();
            //if (user != null)
            //{

            //    if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
            //    else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            //}
            //else
            //{
            //    ViewData["UserHasAdmin"] = "None";

            //}



            return View(post);

        }

        //Delete
        public ActionResult Delete(int id)
        {
            string sql = "delete from feedback where feedbackid = @id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        //Changes publish value to true(1) when button is clicked
        public ActionResult Publish(int id)
        {
            string sql = "UPDATE feedback SET IsPublished = 1 WHERE feedbackid=@id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

        //Changes publish value to false(0) when button is clicked
        public ActionResult Unpublish(int id)
        {
            string sql = "UPDATE feedback SET IsPublished = 0 WHERE feedbackid=@id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }
    }
}