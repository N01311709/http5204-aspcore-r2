﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;


namespace http5204_aspcore_group5_r2.Controllers
{
    public class DonationsController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public DonationsController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;

            _env = env;

            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState:
            //0 => No account whatsoever
            //1 => Is User; Patient Online Services account
            //2 => Is an Admin account
            if (user == null) return 0; // only is a User, not an admin (i.e. Patient account only)
            var userid = user.Id;
            if (user.AdminID == null) return 1; //Only a User; not hospital staff
            else
            {
                return 2; //User has a staff/admin account and is a hospital employee

            }

        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["adminid"] = "False"; }
                else { ViewData["adminid"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["adminid"] = "None";
            }
            return View(db.Donations.ToList());
        }

        [HttpPost]
        public ActionResult Create(int donorID, string donorFName, string donorLName, string donorEmail, string donationPlan, float donationAmount)
        {
            string query = "insert into donations (AdminID, DonationFName, DonationLName, DonationEmail, DonationPlan, DonationAmount) values (@id, @firstname, @lastname, @email, @plan, @amount)";


            SqlParameter[] myparams = new SqlParameter[6];

            myparams[0] = new SqlParameter("@id", donorID);

            myparams[1] = new SqlParameter("@firstname", donorFName);

            myparams[2] = new SqlParameter("@lastname", donorLName);

            myparams[3] = new SqlParameter("@email", donorEmail);

            myparams[4] = new SqlParameter("@plan", donationPlan);

            myparams[5] = new SqlParameter("@amount", donationAmount);


            db.Database.ExecuteSqlCommand(query, myparams);


            return RedirectToAction("list");
        }

        public async Task<ActionResult> List(int pagenum)
        {

            var donationlist = await db.Donations.ToListAsync();
            int countDonations = donationlist.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)countDonations / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["Pagination"] = "";
            if (maxpage > 0)
            {
                ViewData["Pagination"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();   
            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            if (userstate == 2)
            {
                //if user is an admin they can see all donations
                ViewData["UserHasAdmin"] = "True";
                List<Donations> donations = await db.Donations.Skip(start).Take(perpage).ToListAsync();
                return View(donations);
            }
            else
            {
                //if user is any rank other than admin, they will only see their donations and be able to edit their plan
                ViewData["UserHasAdmin"] = "False";
                List<Donations> donations = await db.Donations.Where(d => d.AdminID.Equals(user)).Include(d => d.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(donations);
            }

        }

        //Edit
        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();

            Debug.WriteLine("The donation id I'm looking for is " + id);

            var donation = db.Donations.Find(id);

            if (user != null)
            {

                if (user.AdminID == null) return Forbid();
                else if (user.AdminID != donation.AdminID) return Forbid();
                else
                {
                    Debug.WriteLine("The user Admin ID is " + user.AdminID + " and the author of the donation is " + donation.AdminID);

                    return View(donation);
                }
            }
            else
            {

                return Forbid();
            }


        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, int donorID, string donorFName, string donorLName, string donorEmail, string donationPlan, float donationAmount)
        {

            if (db.Donations.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            //change admin id name in views
            if (user == null) return Forbid();

            if (user.AdminID != donorID) return Forbid();


            string query = "update donations set AdminID=@adminid, DonationFName=@fname, DonationLName=@lname, DonationEmail=@email, DonationPlan=@plan, DonationAmount=@amount where donationid=@id";


            SqlParameter[] myparams = new SqlParameter[7];

            myparams[0] = new SqlParameter("@adminid", donorID);
            myparams[1] = new SqlParameter("@fname", donorFName);
            myparams[2] = new SqlParameter("@lname", donorLName);
            myparams[3] = new SqlParameter("@email", donorEmail);
            myparams[4] = new SqlParameter("@plan", donationPlan);
            myparams[5] = new SqlParameter("@amount", donationAmount);
            myparams[6] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);


            return RedirectToAction("List");
        }

        //Show
        public async Task<ActionResult> Show(int? id)
        {
            if ((id == null) || (db.Donations.Find(id) == null))
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {

                if (user.AdminID == null) { ViewData["adminid"] = "False"; }
                else { ViewData["adminid"] = user.AdminID.ToString(); }

            }
            else
            {
                ViewData["adminid"] = "None";

            }

            SqlParameter myparam = new SqlParameter("@id", id);

            Donations post = db.Donations.Include(d => d.Admin).SingleOrDefault(d => d.DonationID == id);

            return View(post);

        }

        //Delete
        public ActionResult Delete(int id)
        {
            string sql = "delete from donations where donationid = @id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

    }
}