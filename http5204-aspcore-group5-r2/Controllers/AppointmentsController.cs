﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Data;
using http5204_aspcore_group5_r2.Models;
using System.Diagnostics;


namespace http5204_aspcore_group5_r2.Controllers
{
    public class AppointmentsController : Controller
    {

        private readonly NDMHCMSContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AppointmentsController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }
    //=============referance from christine code==================//
        public async Task<ActionResult> Index(int pagenum)
        {
            var Appointment = await db.Appointments.ToListAsync();
            int Appcount = Appointment.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)Appcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Appointments> appointments = await db.Appointments.Skip(start).Take(perpage).ToListAsync();


            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
           
            switch (userstate)
            {

                case 0: return RedirectToAction("Login","Account");
               case 1: return RedirectToAction("Create");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            //return View();
            if (user != null)
            {
          //============ if user is not admin then user should not access admin page=======//
                if (user.AdminID == null) { return View("Create"); }
          //============= if user is admin and has admin id than only user can access admin features========//
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
          //===============to show last id first in table  that's why order by descending============//
                return View(await db.Appointments.OrderByDescending(p => p.AppointmentID).Skip(start).Take(perpage).ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View("Login","Account");
            }
            //return View(await db.Appointments.ToListAsync());
        }

        public ActionResult thanks()
        {
            // help from stackoverflow to show details in thanks message
            // https://stackoverflow.com/questions/14981003/get-the-latest-value-of-table-via-entityframework
            var showData = db.Appointments.OrderByDescending(p => p.AppointmentID).FirstOrDefault();

            return View(showData);
        }
        
        // GET: Appointments/Create
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName");
            return View();
        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AppointmentID,FirstName,LastName,Email,Contact,Address,Postal,DepartmentName,Pdate,Communication")] Appointments appointments)
        {


            //suspect that one of the received values is bad
            //test by printing out each value
            Debug.WriteLine("the appointmentid is " + appointments.AppointmentID + " and the Email is " + appointments.Email);
            if (ModelState.IsValid)
            {
                db.Appointments.Add(appointments);
                db.SaveChanges();
                // var res = await MapUserToAuthor(author); //make account the author

                return RedirectToAction("thanks");
            }
            else
            {
                // ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", appointments.AdminID);
                return RedirectToAction("Create");
            }
        }

        // GET: Appointments/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var appointments = await db.Appointments.SingleOrDefaultAsync(m => m.AppointmentID == id);
            //if (appointments == null)
            //{
            //    return NotFound();
            //}

            Appointments appointments = db.Appointments.Find(id);
            var user = await GetCurrentUserAsync();
            if (appointments == null)
            {
                return NotFound();
            }
            return View(appointments);
        }

        // POST: Appointments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AppointmentID,FirstName,LastName,Email,Contact,Address,Postal,DepartmentName,Pdate,Communication,AdminID")] Appointments appointments)
        {
            if (id != appointments.AppointmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(appointments);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppointmentsExists(appointments.AppointmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", appointments);
            return View(appointments);
        }

        // GET: Appointments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointments = await db.Appointments
                .SingleOrDefaultAsync(m => m.AppointmentID == id);
            if (appointments == null)
            {
                return NotFound();
            }

            return View(appointments);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appointments = await db.Appointments.SingleOrDefaultAsync(m => m.AppointmentID == id);
            db.Appointments.Remove(appointments);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AppointmentsExists(int id)
        {
            return db.Appointments.Any(e => e.AppointmentID == id);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Appointment = await db.Appointments
                .SingleOrDefaultAsync(m => m.AppointmentID == id);
            if (Appointment == null)
            {
                return NotFound();
            }
            return View(Appointment);
        }

        // function which define's weather user is admin or normal user//
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uAdminId = user.AdminID;
                var userEmegrency = await db.Appointments.Where(e => e.AppointmentID == uAdminId).ToListAsync();
                var userEmergecnycount = userEmegrency.Count();
                if (userEmergecnycount == 0) return 2;
                else if (userEmergecnycount > 0) return 3;
            }
            return -1;//something went wrong
        }
        //public async Task<IActionResult> thanks()


        //{

        //    return View(await db.Appointments.ToListAsync());
        //}
    }
}
