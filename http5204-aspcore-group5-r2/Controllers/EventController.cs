﻿//REF Chrsitine code : blog asp.net core
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class EventController : Controller
    {

        private readonly NDMHCMSContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public EventController(NDMHCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        /* public IActionResult Index()
         {
             return View();
         }
         */

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1; 
            else
            {
                var uAdminId = user.AdminID;
                var userEvents = await db.Events.Where(e => e.AdminID == uAdminId).ToListAsync();
                var userEventcount = userEvents.Count();
                if (userEventcount == 0) return 2;
                else if (userEventcount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserEventOwner(ApplicationUser user, int EventID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;
         
            var userEvents = await
                db
                .Events
                .Include(e => e.Admin)
                .Where(e => e.AdminID == user.AdminID)
                .Where(e => e.EventID == EventID)
                .ToListAsync();
            if (userEvents.Count() > 0) return true;
            return false;
        }

        //Index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        /**************************************User view for the vent feature**********************************************/
        public async Task<ActionResult> List(int pagenum)
        {
            // Pagination
            var _EventPage = await db.Events.ToListAsync();
            int eventCount = _EventPage.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)eventCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<Event> events = await db.Events.Include(e => e.Admin).Skip(start).Take(perpage).ToListAsync();
            return View(events);

        }

        public async Task<ActionResult> Show()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            //return View();
            if (user != null)
             {
                 if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                 else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                 return View(await db.Events.ToListAsync());
             }
             else
             {
                 ViewData["UserHasAdmin"] = "None";
                 return View(await db.Events.ToListAsync());
             }

           // return RedirectToAction("List");
        }


        /*********************************Create new events *********************************************/
        public async Task<ActionResult> Create()
        { 
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(string Event_Name, DateTime Event_Time, DateTime Event_Date, string Event_Location, string Event_Detail, int Event_AdminID)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate < 2) return Forbid();
            if (user.AdminID !=Event_AdminID)
            {
                //someone tried to impersonate a blog user by switching the hidden field ID
                return Forbid();
            }

            //SQL Query  for FAQ
            string query = "insert into Events(EventName,EventTime,EventDate,EventLocation,EventDetail,AdminID) " +
                "values (@name,@etime,@edate,@location,@details,@admin)";

            SqlParameter[] myparams = new SqlParameter[6];
            //for @name paramter
            myparams[0] = new SqlParameter("@name", Event_Name);

            //for @time parameter
            myparams[1] = new SqlParameter("@etime", Event_Time);

            //for @tdate parameter
            myparams[2] = new SqlParameter("@edate", Event_Date);

            // for @location paramter
            myparams[3] = new SqlParameter("@location", Event_Location);

            // for @details paramter
            myparams[4] = new SqlParameter("@details", Event_Detail);

            //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)

            myparams[5] = new SqlParameter("@admin", Event_AdminID);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        /********************************Edit particuler data **************************************************************************/
        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserEventOwner(user, id);
            if (!isValid) return Forbid();


            var edit_event = db.Events.Find(id); //finds that blog
            if (edit_event != null) return View(edit_event);
            else return NotFound();
        }
        [HttpPost]
        public async Task<ActionResult> Edit(int id, string event_name, DateTime event_date, DateTime event_time, string event_location, string event_details)
        {
            if (db.Events.Find(id) == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserEventOwner(user, id);
                
                if (!isValid) return Forbid();

                //Query to edit particuler event
                string query = "update Events set EventName=@name, EventDate=@date,  EventTime=@time, EventLocation=@location " +
                           "where EventID=@id";

                SqlParameter[] myparams = new SqlParameter[6];

                myparams[0] = new SqlParameter("@name", event_name);

                myparams[1] = new SqlParameter("@date", event_date);

                myparams[2] = new SqlParameter("@time", event_time);

                myparams[3] = new SqlParameter("@location", event_location);

                myparams[4] = new SqlParameter("@details", event_details);

                myparams[5] = new SqlParameter("@id", id);

                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

                //GOTO: View/Event/Show.cshtml with paramter 
                return RedirectToAction("Show/" + id);

            }
       
            //something went wrong
            return Forbid();

        }
       /************************************************* Detail view of event  ************************************************************/
           public async Task<ActionResult> Detail(int id)
           {
               if (db.Events.Find(id) == null)
               {
                   return NotFound();

               }
               var user = await GetCurrentUserAsync();
               var userstate = await GetUserDetails(user);
               ViewData["UserState"] = userstate;
               ViewData["IsUserEventOwner"] = false;
               if (userstate == 3)
               {
                   ViewData["IsUserEventOwner"] = await IsUserEventOwner(user, id);
               }
               Event detail_event = await db.Events.SingleOrDefaultAsync(e => e.EventID == id);
               return View(detail_event);

           }
       /*****************************Delete*******************************************/
        public async Task<ActionResult> Delete(int id)
        {
            if (db.Events.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserEventOwner(user, id);
                if (!isValid) return Forbid();
                string query = "delete from Events where EventID = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);

                return RedirectToAction("List");
            }
            //Something went wrong...
            return Forbid();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}

