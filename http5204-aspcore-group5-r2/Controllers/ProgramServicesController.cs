﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;


namespace http5204_aspcore_group5_r2.Controllers
{
    public class ProgramServicesController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;


        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ProgramServicesController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;

            _env = env;

            _userManager = usermanager;

        }


        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0; // only is a User, not an admin (i.e. Patient account only)
            var userid = user.Id;
            if (user.AdminID == null) return 1; //Only a User; not hospital staff
            else
            {
                return 2; //User has a staff/admin account and is a hospital employee

            }

        }

        public async Task<ActionResult> List(int pagenum)
        {
            var Program = await db.programServices.ToListAsync();
            int Appcount = Program.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)Appcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<ProgramServices> Programs = await db.programServices.Skip(start).Take(perpage).ToListAsync();


            var user = await GetCurrentUserAsync();

            //i added
            /*ViewData["UserInfoTest"] = 0;
            if(user != null)
            {
                ViewData["UserInfoTest"] = user.Id;
            }
            */
            if (user != null)
            {
                //if the user has no author let the view know 
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                //if the user has an author then let the view know
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(Programs);
            }
            else
            {
                //tell the view there is no user
                ViewData["UserHasAdmin"] = "None";
                return View(Programs);
            }

            //GOTO Views/Blog/List.cshtml

        }

        // GET: ProgramServices
        public async Task<IActionResult> Index()
            {
            var nDMHCMSContext = db.programServices.Include(p => p.Admin);
            return View(await nDMHCMSContext.ToListAsync());
            }

        // GET: ProgramServices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programServices = await db.programServices
                .Include(p => p.Admin)
                .SingleOrDefaultAsync(m => m.programServiceID == id);
            if (programServices == null)
            {
                return NotFound();
            }

            return View(programServices);
        }

        // GET: ProgramServices/Create
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName");
            return View();
        }

        // POST: ProgramServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("programServiceID,Title,Description,AdminID")] ProgramServices programServices)
        {
            if (ModelState.IsValid)
            {
                db.Add(programServices);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", programServices.AdminID);
            return View(programServices);
        }

        // GET: ProgramServices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programServices = await db.programServices.SingleOrDefaultAsync(m => m.programServiceID == id);
            if (programServices == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", programServices.AdminID);
            return View(programServices);
        }

        // POST: ProgramServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("programServiceID,Title,Description,AdminID")] ProgramServices programServices)
        {
            if (id != programServices.programServiceID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(programServices);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProgramServicesExists(programServices.programServiceID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", programServices.AdminID);
            return View(programServices);
        }

        // GET: ProgramServices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programServices = await db.programServices
                .Include(p => p.Admin)
                .SingleOrDefaultAsync(m => m.programServiceID == id);
            if (programServices == null)
            {
                return NotFound();
            }

            return View(programServices);
        }

        // POST: ProgramServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var programServices = await db.programServices.SingleOrDefaultAsync(m => m.programServiceID == id);
            db.programServices.Remove(programServices);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProgramServicesExists(int id)
        {
            return db.programServices.Any(e => e.programServiceID == id);


        }
        

        public async Task<ActionResult> UserIndex(int pagenum)
        {
            var Program = await db.programServices.ToListAsync();
            int Appcount = Program.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)Appcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                //if the user has no author let the view know 
                if (user.AdminID == null)
                {
                    List<ProgramServices> Programs = await db.programServices.Skip(start).Take(perpage).ToListAsync();
                    return View(Programs);
                }
                //if the user has an author then let the view know
                else
                {
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                    return RedirectToAction("Index");
                }
                
            }
            else
            {

                //tell the view there is no user
                ViewData["UserHasAdmin"] = "None";

                
                List<ProgramServices> Programs = await db.programServices.Skip(start).Take(perpage).ToListAsync();
                return View(Programs);
            }
        }

        // GET: ProgramServices/Details/5
        public async Task<IActionResult> UserDetail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programServices = await db.programServices
                .Include(p => p.Admin)
                .SingleOrDefaultAsync(m => m.programServiceID == id);
            if (programServices == null)
            {
                return NotFound();
            }

            return View(programServices);
        }
    }
}
