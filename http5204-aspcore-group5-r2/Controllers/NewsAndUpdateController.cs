﻿//REf https://stackoverflow.com/questions/40447338/adding-images-to-asp-net-core
// REF Chrsitine code : blog asp.net core
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;


namespace http5204_aspcore_group5_r2.Controllers
{
    public class NewsAndUpdateController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;  
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public NewsAndUpdateController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uAdminId = user.AdminID;
                var userNewsUpdate = await db.NewsAndUpdates.Where(e => e.AdminID == uAdminId).ToListAsync();
                var userNewsUpdatecount = userNewsUpdate.Count();
                if (userNewsUpdatecount == 0) return 2;
                else if (userNewsUpdatecount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserNewsUpdateOwner(ApplicationUser user, int NewsUpdateID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;

            var userNewsUpdate = await
                db
                .NewsAndUpdates
                .Include(nu => nu.Admin)
                .Where(nu => nu.AdminID == user.AdminID)
                .Where(nu => nu.NewsUpdateID == NewsUpdateID)
                .ToListAsync();
            if (userNewsUpdate.Count() > 0) return true;
            return false;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        /**************************************User view for the News feature**********************************************/
        public async Task<ActionResult> List(int pagenum)
        {

            // Pagination
            var _NewsUpdatePage = await db.NewsAndUpdates.ToListAsync();
            int newsUpdateCount = _NewsUpdatePage.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)newsUpdateCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<NewsAndUpdate> newsUpdate = await db.NewsAndUpdates.Include(e => e.Admin).Skip(start).Take(perpage).ToListAsync();
            return View(newsUpdate);

            /*   var user = await GetCurrentUserAsync();
               if (user != null)
               {
                   if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                   else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                   return View(newsUpdate);
               }
               else
               {
                   //tell the view there is no user
                   ViewData["UserHasAdmin"] = "None";
                   return View(newsUpdate);
           }*/
        }
        /*************************************Admin table view (Only admin can see this view after he logges in)***********************************************************/
        public async Task<ActionResult> Show()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            //return View();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.NewsAndUpdates.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.NewsAndUpdates.ToListAsync());
            }

            // return RedirectToAction("List");
        }

        /*********************************Create new News and update *********************************************/
        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            ViewData["UserHasAdmin"] = user.AdminID;
            return View();
        }

        [HttpPost]//IFormFile newsImage,
        public async Task<ActionResult> Create(string NewsUpdate_Title, string NewsUpdate_Text,  int NewsUpdate_AdminID)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate < 2) return Forbid();
            if (user.AdminID != NewsUpdate_AdminID)
            {
                //someone tried to impersonate a blog user by switching the hidden field ID
                return Forbid();
            }
            // for image

            //SQL Query  for NewsUpate
            string query = "insert into NewsAndUpdates(NewsUpdateTitle,NewsUpdateImage,NewsUpdateDescription,AdminID) " +
                 "values (@title,@newsImage,@text,@admin)";
            //   Debug.WriteLine("I am trying to check admin" + NewsUpdate_AdminID);
            SqlParameter[] myparams = new SqlParameter[4];
            //for @name paramter
            myparams[0] = new SqlParameter("@title", NewsUpdate_Title);
            //for @time parameter
            myparams[1] = new SqlParameter("@text", NewsUpdate_Text);
            //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)
            myparams[2] = new SqlParameter("@admin", NewsUpdate_AdminID);
            // for Image
            int NewsUpdateImage = 0;
            myparams[3] = new SqlParameter("@newsImage", NewsUpdateImage);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show");

        }
/********************************Edit particuler data **************************************************************************/

        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserNewsUpdateOwner(user, id);
            if (!isValid) return Forbid();
            var mypost = db.NewsAndUpdates.Find(id);
            if (mypost != null) return View(mypost);
            else return NotFound();

        }
        // for Edit Announcement

        [HttpPost]
        public async Task<ActionResult> Edit(int id,string News_update_title,string News_description, IFormFile News_Image)
        {
            Debug.WriteLine(News_Image.Length);

            if (db.NewsAndUpdates.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserNewsUpdateOwner(user, id);
                if (!isValid) return Forbid();

                var NewsUpdateImage = 0;
                var ImgType = "";
                var webRoot = _env.WebRootPath;
                if (News_Image != null)
                {
                    if (News_Image.Length > 0)
                    {
                        Debug.WriteLine("length valid");
                        var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                        var extension = Path.GetExtension(News_Image.FileName).Substring(1);

                        if (valtypes.Contains(extension))
                        {

                            //generic .img extension, web translates easily.
                            string fn = id + "." + extension;

                            //get a direct file path to images/
                            Debug.WriteLine("Image isnt copied");
                            string path = Path.Combine(webRoot,"images/News/");
                            path = Path.Combine(path, fn);

                            //saving the file
                            Debug.WriteLine("It doesnt create an image");
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                Debug.WriteLine("Its not copying it here");
                                News_Image.CopyTo(stream);
                            }
                            //let the model know that there is a picture with an extension
                            Debug.WriteLine("Its not telling the model");
                            NewsUpdateImage = 1;
                            ImgType = extension;

                        }


                    }
                }
                //Raw Update MSSQL query
                string query = "update NewsAndUpdates set NewsUpdateTitle=@title, NewsUpdateDescription=@description,  NewsUpdateImage=@pic, ImgType=@Type where NewsUpdateID=@id";


                SqlParameter[] myparams = new SqlParameter[5];
             
                myparams[0] = new SqlParameter("@title", News_update_title);
             
                myparams[1] = new SqlParameter("@description", News_description);

                myparams[2] = new SqlParameter("@id", id);

                myparams[3] = new SqlParameter("@pic", NewsUpdateImage);

                myparams[4] = new SqlParameter("@Type", ImgType);

                db.Database.ExecuteSqlCommand(query, myparams);

            
                return RedirectToAction("Show/" + id);
            }
            //something went wrong
            return Forbid();

        }








        /***************************Detail of News And Updates ****************************************/
        public async Task<ActionResult> Detail(int id)
        {
            if (db.NewsAndUpdates.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["IsUserNewsUpdateOwner"] = false;
            if (userstate == 3)
            {
                ViewData["IsUserNewsUpdateOwner"] = await IsUserNewsUpdateOwner(user, id);
            }
            NewsAndUpdate detail_newsUpdate = await db.NewsAndUpdates.SingleOrDefaultAsync(n => n.NewsUpdateID == id);
            return View(detail_newsUpdate);

        }
        /*****************************Delete*******************************************/
        public async Task<ActionResult> Delete(int id)
        {
            if (db.NewsAndUpdates.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserNewsUpdateOwner(user, id);
                if (!isValid) return Forbid();
                string query = "delete from  NewsAndUpdates where NewsUpdateID = @id";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);

                return RedirectToAction("List");
            }
            //Something went wrong...
            return Forbid();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
