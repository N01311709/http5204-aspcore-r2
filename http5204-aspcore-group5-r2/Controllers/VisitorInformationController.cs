﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
//using http5204_aspcore_group5_r2.Models.ViewModelMargi;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;

//Christine's Reference
namespace http5204_aspcore_group5_r2.Controllers
{
    public class VisitorInformationController : Controller
    {
        private readonly NDMHCMSContext db;

      
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public VisitorInformationController(NDMHCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }


        /* public IActionResult Index()
         {
             return View();
         }
         */

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an Admin or not.
            //UserState
            //0 => No User
            //1 => has user has no admin
            //2 => has user has admin but no VisitorInformations
            //3 => has user has admin and at least 1 VisitorInformation
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uadminid = user.AdminID;
                var uservisitorinformations = await db.VisitorInformations.Where(v => v.AdminID == uadminid).ToListAsync();
                var uservisitorinfocount = uservisitorinformations.Count();
                if (uservisitorinfocount == 0) return 2;
                else if (uservisitorinfocount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserVisitorInformationsOwner(ApplicationUser user, int VisitorID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;
           
            var uservisitorinfos = await
                db
                .VisitorInformations
                .Include(v => v.Admin)
                .Where(v => v.AdminID == user.AdminID)
                .Where(v => v.VisitorID == VisitorID)
                .ToListAsync();
            if (uservisitorinfos.Count() > 0) return true;
            return false;
        }

        //Index
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //List
        /*  public ActionResult List()
          {
              List<VisitorInformation> visitorInformations = db.VisitorInformations.Include(v => v.Admin).ToList();

              return View(visitorInformations);
          } */

        public async Task<ActionResult> List(int pagenum)
        {
            //i added more functionality in visitor information /****/
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //tell give us context about the user
            ViewData["UserState"] = userstate;
            ViewData["UserHasAdmin"] = 0; //default 0
            if (userstate == 3)
            {
                ViewData["UserHasAdmin"] = (int)user.AdminID;
            }

            //Pagination
            var listvisitorinformations = await db.VisitorInformations.Include(v => v.Admin).ToListAsync();
            int visitorinfocount = listvisitorinformations.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)visitorinfocount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<VisitorInformation> visitorinformations = await db.VisitorInformations.Include(v => v.Admin).Skip(start).Take(perpage).ToListAsync();


            //i added more functionality in visitor information /****/
            //var user = await GetCurrentUserAsync();
            //if (user != null)
            //{
            //    //if the user has no admin let the view know 
            //    if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
            //    //if the user has an admin then let the view know
            //    else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            //    return View(visitorinformations);
            //}
            //else
            //{
            //    //tell the view there is no admin
            //    ViewData["UserHasAdmin"] = "None";
            //    return View(visitorinformations);
            //}
            return View(visitorinformations);
        }

        

        public async Task<ActionResult> New()
        {
            
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
    
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
           
            ViewData["UserHasAdmin"] = user.AdminID;
           
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> New(string VisitorName_New, string VisitingPersonName_New, DateTime Date_New, DateTime TimeIn_New, int VisitorInfoAdmin_New)
        {
            
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

           
            if (userstate < 2) return Forbid();
            if (user.AdminID != VisitorInfoAdmin_New)
            {
               
                return Forbid();
            }

            //for date 
            //DateTime da = DateTime.Now;
            //string Date = da.ToString("mm/dd/yyyy");
            //Raw Query   
            string query = "insert into visitorinformations (VisitorName, VisitingPersonName, Date, TimeIn, AdminID) values (@name, @visitingpersonname, @date, @timein, @admin)";

            SqlParameter[] myparams = new SqlParameter[5];
            //for @name paramter
            myparams[0] = new SqlParameter("@name", VisitorName_New);
            //for @visitingpersonname parameter
            myparams[1] = new SqlParameter("@visitingpersonname", VisitingPersonName_New);
            //for @date paramter 
            myparams[2] = new SqlParameter("@date", Date_New);// DateTime da = DateTime.Now; string date = da.ToString("dd/MM/YYYY");
                                                              //for @timein paramter 
            myparams[3] = new SqlParameter("@timein", TimeIn_New);
            //for @timeout paramter 
            //myparams[4] = new SqlParameter("@timeout", TimeOut_New);
            //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)
            myparams[4] = new SqlParameter("@admin", VisitorInfoAdmin_New);

            
            db.Database.ExecuteSqlCommand(query, myparams);

           
            return RedirectToAction("List");
        }

      
        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserVisitorInformationsOwner(user, id);
            if (!isValid) return Forbid();


            var myvisitorinfo = db.VisitorInformations.Find(id); //finds that blog
            if (myvisitorinfo != null) return View(myvisitorinfo);
            else return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, string VisitorName, string VisitingPersonName, DateTime Date, DateTime TimeIn, DateTime TimeOut, int VisitorInfoAdmin)
        {
           
            if (db.VisitorInformations.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserVisitorInformationsOwner(user, id);
                if (!isValid) return Forbid();
                //something is right

                //Raw Update MSSQL query
                string query = "update visitorinformations set VisitorName=@name, VisitingPersonName=@visitingpersonname, Date=@date, TimeIn=@timein, TimeOut=@timeout, AdminID=@admin where visitorid=@id";

                //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
                //[new SqlParameter[1]] => Create a new SqlParameter array with 7 items
                SqlParameter[] myparams = new SqlParameter[7];
                //Parameter for @name "visitor name"
                myparams[0] = new SqlParameter("@name", VisitorName);
                //Parameter for @visitingpersonname "blogtitle"
                myparams[1] = new SqlParameter("@visitingpersonname", VisitingPersonName);
                //Parameter for @date "date"
                myparams[2] = new SqlParameter("@date", Date);
                //Parameter for @timein "timein"
                myparams[3] = new SqlParameter("@timein", TimeIn);
                //Parameter for @timeout "timeout"
                myparams[4] = new SqlParameter("@timeout", TimeOut);
                //Parameter for (admin) id FOREIGN KEY
                myparams[5] = new SqlParameter("@admin", VisitorInfoAdmin);
                //Pararameter for (visitorid) id PRIMARY KEY
                myparams[6] = new SqlParameter("@id", id);

                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

                return RedirectToAction("Show/" + id);
            }
            //something went wrong
            return Forbid();

        }

        //for Show Visitor Informration(Particular One)
        public async Task<ActionResult> Show(int? id)
        {
            
            if ((id == null) || (db.VisitorInformations.Find(id) == null))
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                //if the user has no admin let the view know 
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                //if the user has an admin then let the view know
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            }
            else
            {
                //tell the view there is no user
                ViewData["UserHasAdmin"] = "None";

            }

            //Raw MSSQL query
            string query = "select * from visitorinformations where visitorid=@id";


            SqlParameter myparam = new SqlParameter("@id", id);

            VisitorInformation myvisitorinfo = db.VisitorInformations.Include(v => v.Admin).SingleOrDefault(v => v.VisitorID == id);

            return View(myvisitorinfo);

        }

        
        public async Task<ActionResult> Delete(int id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if (db.VisitorInformations.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserVisitorInformationsOwner(user, id);
                if (!isValid) return Forbid();
                
                string query = "delete from visitorinformations where visitorid=@id";
                //SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));
                //await db.Database.ExecuteSqlCommandAsync(query, param);
                return RedirectToAction("List");
            }
            //Something is going to wrong...
            return Forbid();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
    }