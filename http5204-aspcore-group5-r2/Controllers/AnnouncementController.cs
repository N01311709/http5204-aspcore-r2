﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
//using http5204_aspcore_group5_r2.Models.ViewModelMargi;
using http5204_aspcore_group5_r2.Data;*/// Extra Reference for Annoucenment Image : https://stackoverflow.com/questions/40447338/adding-images-to-asp-net-core

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

using Microsoft.EntityFrameworkCore; //margi added refernec: https://docs.microsoft.com/en-us/dotnet/api/microsoft.entityframeworkcore.dbset-1.addasync?view=efcore-2.0

//Christine's Reference
namespace http5204_aspcore_group5_r2.Controllers
{
    public class AnnouncementController : Controller
    {
        private readonly NDMHCMSContext db;

        //margi added
        private readonly IHostingEnvironment _env; //for File's Image Path 
        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AnnouncementController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        /* public AnnouncementController(NDMHCMSContext context)
         {
             db = context;
         }
         */


        /* public IActionResult Index()
         {
             return View();
         }*/

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an Admin or not.
            //UserState
            //0 => No User
            //1 => has user has no admin
            //2 => has user has admin but no Announcements
            //3 => has user has admin and at least 1 Announcement
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uadminid = user.AdminID;
                var userannouncements = await db.Announcements.Where(p => p.AdminID == uadminid).ToListAsync();
                var userannouncemnetcount = userannouncements.Count();
                if (userannouncemnetcount == 0) return 2;
                else if (userannouncemnetcount > 0) return 3;
            }
            return -1;
        }

        public async Task<bool> IsUserAnnouncementOwner(ApplicationUser user, int AnnouncementID)
        {
            if (user == null) return false;
            if (user.AdminID == null) return false;
            //purpose of this function is to check whether the user is the admin of the announcement or not
            var userposts = await
                db
                .Announcements
                .Include(p => p.Admin)
                .Where(p => p.AdminID == user.AdminID)
                .Where(p => p.AnnouncementID == AnnouncementID)
                .ToListAsync();
            if (userposts.Count() > 0) return true;
            return false;
        }


         public ActionResult Index()
         {
             return RedirectToAction("List");
         } 

        //List
        /*   public ActionResult List()
           {

               List<Announcement> announcements = db.Announcements.Include(p => p.Admin).ToList();

               return View(announcements);
           }*/

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //tell give us context about the user
            ViewData["UserState"] = userstate;
            ViewData["UserHasAdmin"] = 0; //default 0
            if (userstate == 3)
            {
                ViewData["UserHasAdmin"] = (int)user.AdminID;
            }

            //Pagination
            var listannouncements = await db.Announcements.Include(p => p.Admin).ToListAsync();
            int announcementcount = listannouncements.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)announcementcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Announcement> announcements = await db.Announcements.Include(p => p.Admin).Skip(start).Take(perpage).ToListAsync();



            /* var user = await GetCurrentUserAsync();
             if (user != null)
             {
                 //if the user has no admin let the view know 
                 if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                 //if the user has an admin then let the view know
                 else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                 return View(announcements);
             }
             else
             {
                 //tell the view there is no admin
                 ViewData["UserHasAdmin"] = "None";
                 return View(announcements);
             }*/
            return View(announcements);
        }

        
        
    


        public async Task<ActionResult> New()
        {
           
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
           
            switch (userstate)
            {
                case 0: return RedirectToAction("Register", "Account");
                case 1: return RedirectToAction("Create", "Admin");
            }
            
            ViewData["UserHasAdmin"] = user.AdminID;
            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> New(string AnnouncementTitle_New, string AnnouncementDescription_New, string AnnouncementType_New, string DestinationLink_New, int AnnouncementAdmin_New)
        {
            
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

           
            if (userstate < 2) return Forbid();
            if (user.AdminID != AnnouncementAdmin_New)
            {
               
                return Forbid();
            }

            //Raw Query   
            string query = "insert into announcements (AnnouncementTitle, AnnouncementDescription, AnnouncementType, DestinationLink, AdminID, HasPic) values (@title, @description, @type, @link, @admin, @haspic)";

            SqlParameter[] myparams = new SqlParameter[6];
            //@title paramter
            myparams[0] = new SqlParameter("@title", AnnouncementTitle_New);
            //@description parameter
            myparams[1] = new SqlParameter("@description", AnnouncementDescription_New);
            //@type parameter
            myparams[2] = new SqlParameter("@type", AnnouncementType_New);
            //@link (Destination Link) parameter
            myparams[3] = new SqlParameter("@link", DestinationLink_New);
            //@admin (id) FOREIGN KEY paramter for AnnouncementAdmin
            myparams[4] = new SqlParameter("@admin", AnnouncementAdmin_New);
            int haspic = 0;
            myparams[5] = new SqlParameter("@haspic", haspic);


            db.Database.ExecuteSqlCommand(query, myparams);

          
            return RedirectToAction("List");
        }

        //for show view

        public async Task<ActionResult> Show(int id)
        {
            /*
            if ((id == null) || (db.Announcements.Find(id) == null))
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                //if the user has no author let the view know 
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                //if the user has an author then let the view know
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            }

            else
            {
                //tell the view there is no user
                ViewData["UserHasAdmin"] = "None";

            }


            //Raw MSSQL query
            string query = "select * from announcements where AnnouncementID=@id";


            SqlParameter myparam = new SqlParameter("@id", id);
            */
            //margi comented this for more functionality on show page for edit and delete button*/

            if (db.Announcements.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["IsUserAnnouncementOwner"] = false;
            if (userstate == 3)
            {
                ViewData["IsUserAnnouncementOwner"] = await IsUserAnnouncementOwner(user, id);
            }

            Announcement myannouncement = db.Announcements.Include(p => p.Admin).SingleOrDefault(p => p.AnnouncementID == id);

            return View(myannouncement);


        }


        // for Edit Announcement

        public async Task<ActionResult> Edit(int id)
        {
           

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserAnnouncementOwner(user, id);
            if (!isValid) return Forbid();


            var mypost = db.Announcements.Find(id); //finds that blog
            if (mypost != null) return View(mypost);
            else return NotFound();

        }


        [HttpPost]
        public async Task<ActionResult> Edit(int id, string AnnouncementTitle, string AnnouncementDescription, string AnnouncementType, string DestinationLink, int AnnouncementAdmin, IFormFile announcementimg)
        {
           // Debug.WriteLine(announcementimg.Length);
            
            if (db.Announcements.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserAnnouncementOwner(user, id);
                if (!isValid) return Forbid();






                var haspic = 0;
                var imgtype = "";
                var webRoot = _env.WebRootPath;
                if (announcementimg != null)
                {
                    if (announcementimg.Length > 0)
                    {
                        Debug.WriteLine("length valid");
                        var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                        var extension = Path.GetExtension(announcementimg.FileName).Substring(1);

                        if (valtypes.Contains(extension))
                        {

                            //generic .img extension, web translates easily.
                            string fn = id + "." + extension;

                            //get a direct file path to images/ for announcement
                            string path = Path.Combine(webRoot, "images/Announcement/");
                            path = Path.Combine(path, fn);

                            //saving the file
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                announcementimg.CopyTo(stream);
                            }
                            //let the model know that there is a picture with an extension
                            haspic = 1;
                            imgtype = extension;

                        }

                        
                    }
                }
                //Raw Update MSSQL query
                string query = "update announcements set AnnouncementTitle=@title, AnnouncementDescription=@description, AnnouncementType=@type, DestinationLink=@link, AdminID=@admin, haspic=@haspic, imgtype=@imgtype where AnnouncementID=@id";


                SqlParameter[] myparams = new SqlParameter[8];
                //@title paramter
                myparams[0] = new SqlParameter("@title", AnnouncementTitle);
                //@description parameter
                myparams[1] = new SqlParameter("@description", AnnouncementDescription);
                //@type parameter
                myparams[2] = new SqlParameter("@type", AnnouncementType);
                //@link (Destination Link) parameter
                myparams[3] = new SqlParameter("@link", DestinationLink);
                //@admin (id) FOREIGN KEY paramter for AnnouncementAdmin
                myparams[4] = new SqlParameter("@admin", AnnouncementAdmin);
                //@id parameter
                myparams[5] = new SqlParameter("@id", id);
                //@haspic parameter
                myparams[6] = new SqlParameter("@haspic", haspic);
                //imagetype parameter
                myparams[7] = new SqlParameter("@imgtype", imgtype);




                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

                 
                    return RedirectToAction("Show/" + id);
            }
            
            return Forbid();

        }

    


        

        public async Task<ActionResult> Delete(int id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if (db.Announcements.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserAnnouncementOwner(user, id);
                if (!isValid) return Forbid();
              

                //delete Announcements
                string query = "delete from announcements where AnnouncementID = @id";
                db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

                return RedirectToAction("List");
            }
            //If Something wrong happen
            return Forbid();
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}