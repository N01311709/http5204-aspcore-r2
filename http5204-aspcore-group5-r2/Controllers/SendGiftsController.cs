﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Data;
using http5204_aspcore_group5_r2.Models;
using System.Diagnostics;



namespace http5204_aspcore_group5_r2.Controllers
{
    public class SendGiftsController : Controller
    {
        private readonly NDMHCMSContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public SendGiftsController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        //public async Task<ActionResult> Index()
        //{
        //    return View(await db.SendGifts.ToListAsync());
        //}

        public async Task<ActionResult> Index(int pagenum)
        {
            var Gift = await db.SendGifts.ToListAsync();
            int Appcount = Gift.Count();
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)Appcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<SendGift> sendGifts = await db.SendGifts.OrderByDescending(p => p.SendGiftID).Skip(start).Take(perpage).ToListAsync();


            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            switch (userstate)
            {
                case 0: return RedirectToAction("Login", "Account");
                case 1: return RedirectToAction("Create");
            }

            ViewData["UserHasAdmin"] = user.AdminID;
            if (user != null)
            {
                //============ if user is not admin then user should not access admin page=======//
                if (user.AdminID == null) { return View("Create"); }
                // === if user is admin and has admin id than only user can access admin features ===//
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.SendGifts.Skip(start).Take(perpage).ToListAsync());
            }
             else
            {
                ViewData["UserHasAdmin"] = "None";
                return View("Login", "Account");
            }
           // return View(await db.Appointments.ToListAsync());
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            if (user == null) return 0;
            var userID = user.Id;
            if (user.AdminID == null) return 1;
            else
            {
                var uAdminId = user.AdminID;
                var usergift = await db.SendGifts.Where(e => e.SendGiftID == uAdminId).ToListAsync();
                var usergiftcount = usergift.Count();
                if (usergiftcount == 0) return 2;
                else if (usergiftcount > 0) return 3;
            }
            return -1;//something went wrong
        }
        //============for thansk message page=======================//
        public ActionResult ThanksMsg()
        {
        // help from stackoverflow to show details in thanks message
        // https://stackoverflow.com/questions/14981003/get-the-latest-value-of-table-via-entityframework
        var showPiece = db.SendGifts.OrderByDescending(p => p.SendGiftID).FirstOrDefault();

        return View(showPiece);
        }


        //// GET: SendGifts/Create
        
// ===========to get new request from user=======//
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create(int SendGiftID, string senderFname, string senderLname,string Email, string Contact, string patientName,int roomNo, int amt,string message )
        //{


        // string query = "insert into SendGift (SendGiftID,senderFname,senderLname,Email,Contact,patientName,roomNo,amt,message,AdminID) " +
        // "values (@senderFname,@senderLname,@Email,@contact,@patientName,@roomNo,@message,@admin)";

        //        SqlParameter[] myparams = new SqlParameter[9];
        //        myparams[0] = new SqlParameter("@SendGiftID", SendGiftID);
        //        myparams[1] = new SqlParameter("@senderFname", senderFname);
        //        myparams[2] = new SqlParameter("@senderLname", senderLname);
        //        myparams[3] = new SqlParameter("@Email", Email);
        //        myparams[4] = new SqlParameter("@Contact", Contact);
        //        myparams[5] = new SqlParameter("@patientName", patientName);
        //        myparams[6] = new SqlParameter("@roomNo", roomNo);
        //        myparams[7] = new SqlParameter("@message", message);
        //        //for @admin (id) FOREIGN KEY paramter (AdminId Foreign Key)
        //        //myparams[8] = new SqlParameter("@admin", AdminID);
        //        //db.Database.ExecuteSqlCommand(query, myparams);
        //            return RedirectToAction("Index");
        //}


        public async Task<IActionResult> Create([Bind("SendGiftID,senderFname,senderLname,Email,Contact,patientName,roomNo,amt,message,AdminID")] SendGift sendGift)
        {
            if (ModelState.IsValid)
            {
                db.SendGifts.Add(sendGift);
                db.SaveChanges();
                await db.SaveChangesAsync();
                return RedirectToAction("ThanksMsg");
            }
            else
            {
                //ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", sendGift.AdminID);
                return RedirectToAction("Create");
            }
        }

        // GET: SendGifts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
                SendGift sendGift = db.SendGifts.Find(id);
                var user = await GetCurrentUserAsync();
                if (sendGift == null)
                {
                    return NotFound();
                }
                return View(sendGift);
        }

        // POST: SendGifts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SendGiftID,senderFname,senderLname,Email,Contact,patientName,roomNo,amt,message")] SendGift sendGift)
        {
            if (id != sendGift.SendGiftID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(sendGift);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
   //=============if send gift id is null=========================// 
                    if (!SendGiftExists(sendGift.SendGiftID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AdminID"] = new SelectList(db.Admin, "AdminID", "AdminFName", sendGift);
            return View(sendGift);
        }

          // GET: SendGifts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sendGift = await db.SendGifts
                .SingleOrDefaultAsync(m => m.SendGiftID == id);
            if (sendGift == null)
            {
                return NotFound();
            }
            return View(sendGift);
        }

        // POST: SendGifts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sendGift = await db.SendGifts.SingleOrDefaultAsync(m => m.SendGiftID == id);
            db.SendGifts.Remove(sendGift);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sendGift = await db.SendGifts
                .SingleOrDefaultAsync(m => m.SendGiftID == id);
            if (sendGift == null)
            {
                return NotFound();
            }
            return View(sendGift);
        }


        // return RedirectToAction("List");    
        private bool SendGiftExists(int id)
        {
            return db.SendGifts.Any(e => e.SendGiftID == id);
        }
    }
}
