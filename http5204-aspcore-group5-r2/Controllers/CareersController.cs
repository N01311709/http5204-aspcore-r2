﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using System.Diagnostics;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class CareersController : Controller
    {

        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public CareersController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;

            _env = env;

            _userManager = usermanager;
           
        }

        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //UserState:
            //0 => No account whatsoever
            //1 => Is User; Patient Online Services account
            //2 => Is an Admin account
            if (user == null) return 0; // only is a User, not an admin (i.e. Patient account only)
            var userid = user.Id;
            if (user.AdminID == null) return 1; //Only a User; not hospital staff
            else
            {
                return 2; //User has a staff/admin account and is a hospital employee

            }

        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(db.Careers.ToList());
        }

        [HttpPost]
        public ActionResult Create(string NewCareerTitle, string NewCareerDesc, string CareerDep, string NewCareerDoc, int CareerAdmin)
        {
            string query = "insert into careers (CareerTitle, CareerDesc, CareerDepartment, ResumeUpload, AdminID) values (@title, @desc, @dep, @doc, @admin)";

          
            SqlParameter[] myparams = new SqlParameter[5];
          
            myparams[0] = new SqlParameter("@title", NewCareerTitle);
          
            myparams[1] = new SqlParameter("@desc", NewCareerDesc);

            myparams[2] = new SqlParameter("@dep", CareerDep);
         
            myparams[3] = new SqlParameter("@doc", NewCareerDoc);

            myparams[4] = new SqlParameter("@admin", CareerAdmin);

          
            db.Database.ExecuteSqlCommand(query, myparams);

           
            return RedirectToAction("List");
        }



        public async Task<ActionResult> List(int pagenum)
        {
            var careersList = await db.Careers.ToListAsync();
            int countJobs = careersList.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)countJobs / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["Pagination"] = "";
            if (maxpage > 0)
            {
                ViewData["Pagination"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }



            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            if (userstate == 2)
            {
                ViewData["UserHasAdmin"] = "True";
                List<Careers> careers = await db.Careers.Skip(start).Take(perpage).ToListAsync();
                return View(careers);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                List<Careers> careers = await db.Careers.Include(c => c.Admin).Skip(start).Take(perpage).ToListAsync();
                return View(careers);
            }

        }

        

        public async Task<ActionResult> Edit(int id)
        {

            var user = await GetCurrentUserAsync();
       
            Debug.WriteLine("The blog im trying to find is of id " + id);

            var career = db.Careers.Find(id);
            Debug.WriteLine("This blog's name is" + career);

            if (user != null)
            {
             
                if (user.AdminID == null) return Forbid();
                else if (user.AdminID != career.AdminID) return Forbid();
                else
                {
                    Debug.WriteLine("The user Admin ID is " + user.AdminID + " and the author of the posting is " + career.AdminID);
                 
                    return View(career);
                }
            }
            else
            {
       
                return Forbid();
            }


        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, string NewCareerTitle, string NewCareerDesc, string NewCareerDoc, int CareerAdmin)
        {
         
            if (db.Careers.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
           
            if (user == null) return Forbid();
          
            if (user.AdminID != CareerAdmin) return Forbid();

           
            string query = "update careers set CareerTitle=@title, CareerDesc=@desc, AdminID=@admin where careerid=@id";

           
            SqlParameter[] myparams = new SqlParameter[4];
       
            myparams[0] = new SqlParameter("@title", NewCareerTitle);
          
            myparams[1] = new SqlParameter("@desc", NewCareerDesc);
            
            myparams[2] = new SqlParameter("@admin", CareerAdmin);

            myparams[3] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            
            return RedirectToAction("List");
        }

        //Show
        public async Task<ActionResult> Show(int? id)
        {
            if ((id == null) || (db.Careers.Find(id) == null))
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            }
            else
            {
                ViewData["UserHasAdmin"] = "None";

            }

            SqlParameter myparam = new SqlParameter("@id", id);

            Careers post = db.Careers.Include(c => c.Admin).SingleOrDefault(c => c.CareerID == id);

            return View(post);

        }

        //Delete
        public ActionResult Delete(int id)
        {
            string sql = "delete from careers where careerid = @id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }


    }      
}