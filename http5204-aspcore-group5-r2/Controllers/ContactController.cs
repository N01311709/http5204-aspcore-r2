﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using http5204_aspcore_group5_r2.Models;
using http5204_aspcore_group5_r2.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;


namespace http5204_aspcore_group5_r2.Controllers
{
    public class ContactController : Controller
    {
        private readonly NDMHCMSContext db;

        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ContactController(NDMHCMSContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {

            db = context;

            _env = env;

            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }


        public async Task<ActionResult> New()
        {

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(string reason, string message, string name, string phone, string email)
        {
            string query = "insert into Contact (ContactReason, ContactMessage, ContactName, ContactPhone, ContactEmail) " +
                "values (@reason, @message, @name, @phone, @email)";


            SqlParameter[] myparams = new SqlParameter[5];

            myparams[0] = new SqlParameter("@reason", reason);

            myparams[1] = new SqlParameter("@message", message);

            myparams[2] = new SqlParameter("@name", name);

            myparams[3] = new SqlParameter("@phone", phone);

            myparams[4] = new SqlParameter("@email", email);


            db.Database.ExecuteSqlCommand(query, myparams);


            return RedirectToAction("List");
        }

        //pagination
        public async Task<ActionResult> List(int pagenum)
        {
            var ContactList = await db.Contact.ToListAsync();
            int reviews = ContactList.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)reviews / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["Pagination"] = "";
            if (maxpage > 0)
            {
                ViewData["Pagination"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            List<Contact> contact = await db.Contact.Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(contact);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(contact);
            }

        }

        //public ActionResult Edit(int? id)
        //{
        //    Contact contact = new Contact();
        //    contact =
        //        db.Contact.SingleOrDefault(p => p.ContactID == id);
        //    if (contact != null) return View(contact);
        //    else return NotFound();
        //}

        //[HttpPost]
        //public ActionResult Edit(int? id, string reason, string message, string name, string phone, string email)
        //{
        //    if ((id == null) || (db.Contact.Find(id) == null))
        //    {
        //        return NotFound();

        //    }
        //    string query = "update Contact set ContactReason=@reason, " +
        //        "ContactMessage=@message, " +
        //        "ContactName=@name," +
        //        "ContactPhone=@phone," +
        //        "ContactEmail=@email where ContactID = @id";

        //    SqlParameter[] myparams = new SqlParameter[6];
        //    myparams[0] = new SqlParameter();
        //    myparams[0].ParameterName = "@reason";
        //    myparams[0].Value = reason;

        //    myparams[1] = new SqlParameter();
        //    myparams[1].ParameterName = "@message";
        //    myparams[1].Value = message;

        //    myparams[2] = new SqlParameter();
        //    myparams[2].ParameterName = "@name";
        //    myparams[2].Value = name;

        //    myparams[3] = new SqlParameter();
        //    myparams[3].ParameterName = "@phone";
        //    myparams[3].Value = phone;

        //    myparams[4] = new SqlParameter();
        //    myparams[4].ParameterName = "@email";
        //    myparams[4].Value = email;

        //    myparams[5] = new SqlParameter();
        //    myparams[5].ParameterName = "@id";
        //    myparams[5].Value = id;
        //    db.Database.ExecuteSqlCommand(query, myparams);

        //    return RedirectToAction("List");
        //}

        //Delete
        public ActionResult Delete(int id)
        {
            string sql = "delete from Contact where ContactID = @id";
            db.Database.ExecuteSqlCommand(sql, new SqlParameter("@id", id));

            return RedirectToAction("List");
        }

    }
}