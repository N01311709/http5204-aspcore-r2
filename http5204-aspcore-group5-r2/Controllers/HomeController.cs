﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using http5204_aspcore_group5_r2.Models;

namespace http5204_aspcore_group5_r2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["HomePage"] = true;
           // ViewData["NewsDisplay"] = true;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
